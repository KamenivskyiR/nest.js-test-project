import { Injectable } from '@nestjs/common';
import { CreateCityDto } from './dto/create-city.dto';
import { UpdateCityDto } from './dto/update-city.dto';
@Injectable()
export class CitiesService {
  private cities = [];

  insertCity(createCityDto: CreateCityDto) {
    const newCity = this.createCity(createCityDto);

    this.cities = [...this.cities, newCity];
  }

  getAllCities() {
    return this.cities;
  }

  getById(id: number) {
    return this.cities.find((city) => city.id == id);
  }

  update(id: number, updateCityDto: UpdateCityDto) {
    return `This action updates a #${id} city`;
  }

  remove(id: number) {
    return `This action removes a #${id} city`;
  }

  private createCity({ name }) {
    const newCity = {
      name,
      enabled: false,
      marketing: [],
      orders: [],
      id: Math.random().toString(),
    };

    return newCity;
  }
}
