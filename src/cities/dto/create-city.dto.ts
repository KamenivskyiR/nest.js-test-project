export class CreateCityDto {
  id: string;
  name: string;
  fund: IFund;
  enabled: boolean;
  $key?: string;
  products?: {
    [productId: string]: Array<string>;
  };
  marketing: {
    background_img: string;
    message: string;
  };
  orders: {
    [user: string]: Array<string>;
  };
}
export interface IFund {
  total: number;
  funded: number;
}
